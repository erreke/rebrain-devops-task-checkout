# DevOps workshop by Rebrain

Данный проект является репозиторием студента Куандыкова Ербола для практикума DevOps инженеров от Rebrain.

## Начало работы

Следуя данным инструкциям вы можете получить копию проекта, которая будет развернута на вашем локальном компьютере для изучения.

### Установка

Чтобы развернуть проект на вашей машине, надо его склонировать:

```
git clone https://gitlab.rebrainme.com/ereke/rebrain-devops-task-checkout
```

## Запуск тестов

Тесты на данный момент отсутствуют. Возможно появятся в последующих версиях.

## Содействие

Пожалуйста, прочитайте [CONTRIBUTING.md](https://gitlab.rebrainme.com/ereke/rebrain-devops-task-checkout/blob/master/CONTRIBUTING.md) для получения подробной информации о кодексе поведения и процедуре отправки пулл-реквестов.

## Ошибки/Проблемы

Пожалуйста, проверьте [список известных проблем](https://gitlab.rebrainme.com/ereke/rebrain-devops-task-checkout/issues), прежде чем открывать новую. Проблема оформленная в несоответствии с общеустановленными принципами в сообществе, могут быть немедленно закрыты.

## Версионирование

Мы используем [SemVer](http://semver.org/) для управления версиями. Доступные версии см. в [тегах этого хранилища](https://gitlab.rebrainme.com/ereke/rebrain-devops-task-checkout/-/tags).

## Изменения

Подробные изменения для каждого выпуска описаны в [примечаниях к выпуску](https://gitlab.rebrainme.com/ereke/rebrain-devops-task-checkout/-/releases).

## Авторы

**Yerbol Kuandykov** - *ereke@list.ru*

Смотрите также список [участников](https://gitlab.rebrainme.com/ereke/rebrain-devops-task-checkout/-/graphs/master), которые содействовали проекту.

## License

Данный проект лицензирован по лицензии MIT - подробности см. в [LICENSE.md](https://gitlab.rebrainme.com/ereke/rebrain-devops-task-checkout/blob/master/LICENSE.md).
